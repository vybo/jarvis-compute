import timeline_import
import visits
import places
import argsparser
import sys

opts = argsparser.parseArgs(sys.argv[1:])


def get_visits_web(file):
    if file is not None:
        print ("[[Core]]=====================[FILE LOADING]=====================")
        print("Found file. Loading...")
        json_file = open(file).read()
        print ("File loaded into memory.")
        print ("Parsing file into objects")
        timeline = timeline_import.parse_timeline_json(json_file)
        print ("Parsed file")
        print ("[[Core]]====================[JSON > TIMELINE]===================")
        print ("Translating and sorting file...")
        timeline_translated_chronological = timeline_import.translate_timeline_dictionary(timeline['locations'])
        print ("File translated and sorted.")
        print ("[[Core]]===========[VISITS LINKING AND CLASSIFICATION]===========")
        print ("Linking day visits from points...")
        timeline_visits = visits.link_points_into_visits(timeline_translated_chronological)
        print ("Linked day visits.")
        return timeline_visits


def get_visits():
    if "--file" in opts.keys():
        print ("[[Core]]=====================[FILE LOADING]=====================")
        print("Found file. Loading...")
        json_file = open(opts["--file"]).read()
        print ("File loaded into memory.")
        print ("Parsing file into objects")
        timeline = timeline_import.parse_timeline_json(json_file)
        print ("Parsed file")
        print ("[[Core]]====================[JSON > TIMELINE]===================")
        print ("Translating and sorting file...")
        timeline_translated_chronological = timeline_import.translate_timeline_dictionary(timeline['locations'])
        print ("File translated and sorted.")
        print ("[[Core]]===========[VISITS LINKING AND CLASSIFICATION]===========")
        print ("Linking day visits from points...")
        timeline_visits = visits.link_points_into_visits(timeline_translated_chronological)
        print ("Linked day visits.")
        return timeline_visits
    else:
        print ("No file specified\nUsage: timeline_import.py --file \"Path to file\"")


def get_places_web(file):
    timeline_visits = get_visits_web(file)
    print ("[[Core]]=====================[PROCESSING PLACES]=====================")
    analysed_places = places.analyse_places(timeline_visits)
    print ("Got places.")
    return analysed_places


def get_places(timeline_visits):
    print ("[[Core]]=====================[PROCESSING PLACES]=====================")
    analysed_places = places.analyse_places(timeline_visits)
    print ("Got places.")
    return analysed_places


# TODO: Debug for usage without django
#visits = get_visits()
#places = get_places(visits)
