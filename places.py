from geopy.distance import great_circle
from datetime import datetime
import copy
import urllib2
import json


def analyse_places(timeline_visits):
    start_time = datetime.now()
    counter = 0
    warn = 0
    print ("[places]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    all_visits = []
    analysed_places = []

    # Preprocess the data so we have all visits in one single list, it will be easier this way to iterate through
    for year in timeline_visits.keys():
        if year == 'meta':
            pass
        else:
            for month in timeline_visits[year].keys():
                for day in timeline_visits[year][month].keys():
                    for visit in timeline_visits[year][month][day]['visits']:
                        all_visits.append(visit)


    visits_amount = len(all_visits)
    visits_iterable = iter(range(0, visits_amount))
    leftovers = copy.copy(all_visits)
    to_remove = []

    # Link visits into places
    for v in visits_iterable:
        place = {}

        # Assign defaults
        place['name'] = 'unresolved'
        place['type'] = 'other'
        place['count'] = 1
        place['starting_time_count'] = []
        place['ending_time_count'] = []

        # List of counts of visited times, List will always have capacity of 24 items, representing hours in a day
        for i in range(0,24):
            place['starting_time_count'].append(0)
            place['ending_time_count'].append(0)

        place['lengths'] = []
        place['lengths'].append(all_visits[v]['difference_time_debug'])
        place['home_probability'] = 1
        place['work_probability'] = 1
        place['list_of_latitudes'] = []
        place['list_of_longitudes'] = []

        # Local variables
        latitude_1 = all_visits[v]['latitude']
        longitude_1 = all_visits[v]['longitude']
        point1 = (latitude_1, longitude_1)
        # TODO: Only if the length is over few hours
        starting_time_1 = datetime.fromtimestamp(all_visits[v]['starting_time_timestampS'])
        ending_time_1 = datetime.fromtimestamp(all_visits[v]['ending_time_timestampS'])
        starting_hour_1 = starting_time_1.hour
        ending_hour_1 = ending_time_1.hour

        place['starting_time_count'][starting_hour_1] += 1
        place['ending_time_count'][ending_hour_1] += 1
        place['list_of_latitudes'].append(latitude_1)
        place['list_of_longitudes'].append(longitude_1)

        # Remove already processed visits
        for rem in to_remove:
            leftovers.remove(rem)

        # Empty to remove list
        to_remove = []

        # Link any other existing visits with this one, the same principle as in linking visits, however not chronologically dependent
        for v2 in range(v+1, len(leftovers)):
            longitude_2 = leftovers[v2]['longitude']
            latitude_2 = leftovers[v2]['latitude']
            point2 = (latitude_2, longitude_2)
            distance_between = great_circle(point1, point2).meters

            # If the distance is less than x meters, the visit corresponds to the same place
            if distance_between < 80:
                starting_hour_2 = datetime.fromtimestamp(leftovers[v2]['starting_time_timestampS']).hour
                ending_hour_2 = datetime.fromtimestamp(leftovers[v2]['ending_time_timestampS']).hour

                place['list_of_latitudes'].append(latitude_2)
                place['list_of_longitudes'].append(longitude_2)
                place['count'] += 1
                place['starting_time_count'][starting_hour_2] += 1
                place['ending_time_count'][ending_hour_2] += 1
                place['lengths'].append(leftovers[v2]['difference_time_debug'])
                # If the visit corresponds to current place, remove it from all visits, because the visit is already processed
                to_remove.append(leftovers[v2])
            '''
            else:
                # Continue the first for after all the already checked visits, so no duplicate places get created
                try:
                    [visits_iterable.next() for x in range(v2)] # range(p-1, p2)
                except StopIteration:
                    pass
                # Break loop because the places do not relate
                break
            '''
        '''
        else:
            # If the first loop is broken by the second loop, we still need to save the places data
            # Usually happens when the last place is being processed
            if v2+1 == visits_amount:
                # TODO: Proper average once again
                # TODO: Remove duplicate code?
                place['latitude'] = latitude_1
                place['longitude'] = longitude_1
                counter += 1
                analysed_places.append(place)
                print "[places] Processed a visited place"
                break
        '''

        # TODO: Proper average once again
        place['latitude'] = latitude_1
        place['longitude'] = longitude_1
        counter += 1
        analysed_places.append(place)
        print "[places] Processed a visited place"

    print ("[places] Evaluating properties, analysing places.")

    for place in analysed_places:
        # If user visited the place more than X times, it's probable, that it is their place of work or home
        # TODO normalisations
        if place['count'] > 30:
            place['home_probability'] *= 2
            place['work_probability'] *= 2

            url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + str(place['latitude']) + ',' \
                  + str(place['longitude']) + '&key=AIzaSyDOrjHzmO72EceKg7lpDmcu_iI8wqlXUU8'
            address_response = urllib2.urlopen(url).read()
            address_object = json.loads(address_response)
            address_text = address_object['results'][0]['formatted_address']
            place['name'] = address_text

            arrive_night = 0
            arrive_day = 0
            leave_night = 0
            leave_day = 0
            for i in range(len(place['starting_time_count'])):
                # Count the times user arrived to the place at night
                if i > 13 or i < 6:
                    arrive_night += place['starting_time_count'][i]
                # Or the times user arrived to the place at morning
                else:
                    arrive_day += place['starting_time_count'][i]

            for i in range(len(place['ending_time_count'])):
                # Count the times user left the place at night
                if i > 13 and i < 18: # Usually they go out from home at evenings for fun, so a bit of a space is needed
                    leave_night += place['ending_time_count'][i]
                # Or the times user left the place at morning
                elif i > 5 and i < 14:
                    leave_day += place['ending_time_count'][i]

            if arrive_night > arrive_day and leave_day > leave_night:
                place['home_probability'] *= 1.5
            else:
                place['work_probability'] *= 1.5

        else:
            place['home_probability'] *= 0.5
            place['work_probability'] *= 0.5

        print ("[places] Evaluated place.")



    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[places]Found " + str(counter) + " different visited places. Finished in " + str(difference_time_seconds) + ".")

    return analysed_places
