from datetime import datetime
import json


def parse_timeline_json(json_file):
    start_time = datetime.now()
    print ("[timeline_import]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))
    timeline_objects = json.loads(json_file)
    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[timeline_import]Finished in " + str(difference_time_seconds) + ".")
    return timeline_objects


def translate_timeline_dictionary(timeline_list):
    timeline_chronological = {}

    counter = 0
    errors = 0
    start_time = datetime.now()
    print ("[timeline_import]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    for entry in timeline_list:
        timestamp = entry['timestampMs']
        timestamp_int_seconds = float(timestamp) / 1000 # Python is expecting seconds timestamp, not milliseconds.
        time = datetime.fromtimestamp(timestamp_int_seconds)
        entry_year = time.strftime('%Y')
        entry_month = time.strftime('%m')
        entry_day = time.strftime('%d')


        #TODO: Simplified because of debugging purposes. Delete the upcoming ifs for production
        if entry_year != '2015' or not set([entry_month]).issubset(['05', '06', '07']): # or entry_month != '05' or entry_month != '06' or entry_month != '07':
            continue

        # Build dicionaries to a ceratin structure
        if entry_year not in timeline_chronological.keys():
            timeline_chronological[entry_year] = {}
            timeline_chronological[entry_year][entry_month] = {}
            timeline_chronological[entry_year][entry_month][entry_day] = {}
            timeline_chronological[entry_year][entry_month][entry_day]['entries'] = []
            timeline_chronological[entry_year][entry_month][entry_day]['errors'] = []
        elif entry_month not in timeline_chronological[entry_year]:
            timeline_chronological[entry_year][entry_month] = {}
            timeline_chronological[entry_year][entry_month][entry_day] = {}
            timeline_chronological[entry_year][entry_month][entry_day]['entries'] = []
            timeline_chronological[entry_year][entry_month][entry_day]['errors'] = []
        elif entry_day not in timeline_chronological[entry_year][entry_month]:
            timeline_chronological[entry_year][entry_month][entry_day] = {}
            timeline_chronological[entry_year][entry_month][entry_day]['entries'] = []
            timeline_chronological[entry_year][entry_month][entry_day]['errors'] = []

        # Fill them, if the entry in timeline has location data
        if 'latitudeE7' and 'longitudeE7' in entry.keys():
            timeline_chronological[entry_year][entry_month][entry_day]['entries'].append({
                'latitudeE7': entry['latitudeE7'],
                'longitudeE7': entry['longitudeE7'],
                'accuracy': entry['accuracy'],
                'timestampS': timestamp_int_seconds,
                'dateTimeDebug': datetime.fromtimestamp(timestamp_int_seconds).strftime('%Y-%m-%d %H:%M:%S')})
        else:
            timeline_chronological[entry_year][entry_month][entry_day]['errors'].append('Entry discarded, because ' \
                                                                                  'it contained no ' \
                                                                                  'location data.')
            errors += 1

        counter += 1

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[timeline_import]Translated " + str(counter) + " entries in " + str(difference_time_seconds) + ". Errors occured: "
                                                                                          "" + str(errors) + ".")

    start_time = datetime.now()
    print ("[timeline_import]Sorting entries. Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    for year in timeline_chronological.keys():
        for month in timeline_chronological[year].keys():
            for day in timeline_chronological[year][month].keys():
                if len(timeline_chronological[year][month][day]['entries']) > 0:
                    timeline_chronological[year][month][day]['entries'].sort(key=lambda x: x['timestampS'], reverse=False)
                    # Sort entries by timestampS, lower is first

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print ("[timeline_import]Entries sorted in " + str(difference_time_seconds))
    return timeline_chronological