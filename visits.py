from geopy.distance import great_circle
from datetime import datetime
import math


def center_point(list_of_latitudes, list_of_longitudes):
    # Source: http://stackoverflow.com/questions/6671183/calculate-the-center-point-of-multiple-latitude-longitude-coordinate-pairs

    count = len(list_of_latitudes)
    sum_x = 0
    sum_y = 0
    sum_z = 0

    for i in range(len(list_of_latitudes)):
        # Convert to radians
        lat = list_of_latitudes[i] * math.pi/180
        lon = list_of_longitudes[i] * math.pi/180
        # Convert to cartesian coords
        X = math.cos(lat) * math.cos(lon)
        Y = math.cos(lat) * math.sin(lon)
        Z = math.sin(lat)
        # Add to sum
        sum_x += X
        sum_y += Y
        sum_z += Z

    # Average coordinates
    avg_x = sum_x / count
    avg_y = sum_y / count
    avg_z = sum_z / count

    # Convert averages to lat,lon
    avg_lon = math.atan2(avg_y, avg_x)
    hyp = math.sqrt(avg_x * avg_x + avg_y * avg_y)
    avg_lat = math.atan2(avg_z, hyp)

    return [avg_lat, avg_lon]


def link_points_into_visits(timeline_chronological):
    start_time = datetime.now()
    counter = 0
    warn = 0
    print ("[visits]Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    timeline_visits = {}
    timeline_visits['meta'] = {}

    '''
    # This needs to be rewritten, it causes the new structure to not work as expected
    timeline_visits = dict.fromkeys(timeline_chronological.keys(),{})
    for year in timeline_chronological.keys():
        timeline_visits[year] = dict.fromkeys(timeline_chronological[year].keys(),{})
        for month in timeline_chronological[year].keys():
            timeline_visits[year][month] = dict.fromkeys(timeline_chronological[year][month].keys(),{})
            for day in timeline_visits[year][month].keys():
                timeline_visits[year][month][day]['visits'] = []
    '''

    for year in sorted(timeline_chronological):
        if year not in timeline_visits.keys():
            timeline_visits[year] = {}

        for month in sorted(timeline_chronological[year]):
            if month not in timeline_visits[year].keys():
                timeline_visits[year][month] = {}

            for day in sorted(timeline_chronological[year][month]):
                if day not in timeline_visits[year][month].keys():
                    timeline_visits[year][month][day] = {}

                visits = []
                entries_amount = len(timeline_chronological[year][month][day]['entries'])
                entries_iterable = iter(range(0, entries_amount))

                visit = {}

                for p in entries_iterable:
                    visit = {}
                    # Assign default time, so we can later save min and max
                    visit['starting_time_timestampS'] = timeline_chronological[year][month][day]['entries'][p]['timestampS']
                    visit['ending_time_timestampS'] = timeline_chronological[year][month][day]['entries'][p]['timestampS']
                    visit['list_of_latitudes'] = []
                    visit['list_of_longitudes'] = []
                    # Transform data from E7 format to normal floating point coordinates
                    longitude_1 = timeline_chronological[year][month][day]['entries'][p]['longitudeE7'] * 0.0000001
                    latitude_1 = timeline_chronological[year][month][day]['entries'][p]['latitudeE7'] * 0.0000001
                    point1 = (latitude_1, longitude_1)
                    # Add the first point to visit
                    visit['list_of_latitudes'].append(latitude_1)
                    visit['list_of_longitudes'].append(longitude_1)

                    # Check only points after the current one
                    for p2 in range(p+1, entries_amount):
                        longitude_2 = timeline_chronological[year][month][day]['entries'][p2]['longitudeE7'] * 0.0000001
                        latitude_2 = timeline_chronological[year][month][day]['entries'][p2]['latitudeE7'] * 0.0000001
                        point2 = (latitude_2, longitude_2)
                        distance_between = great_circle(point1, point2).meters

                        # If the 2 points relate, they are the same visit
                        if distance_between < 80: # TODO: otestovat s accuracy a jinym nastavenim
                            visit['list_of_latitudes'].append(latitude_2)
                            visit['list_of_longitudes'].append(longitude_2)

                            if visit['starting_time_timestampS'] > timeline_chronological[year][month][day]['entries'][p2]['timestampS']:
                                visit['starting_time_timestampS'] = timeline_chronological[year][month][day]['entries'][p2]['timestampS']
                            elif visit['ending_time_timestampS'] < timeline_chronological[year][month][day]['entries'][p2]['timestampS']:
                                visit['ending_time_timestampS'] = timeline_chronological[year][month][day]['entries'][p2]['timestampS']
                        else:
                            # Continue the first for after all the already checked points, so no duplicate visits get created
                            try:
                                [entries_iterable.next() for x in range(p2)] # range(p-1, p2)
                            except StopIteration:
                                pass
                            # Break loop because the visits are not chronologically joined, thus do not relate
                            break
                    else:
                        # When the p2 loop finishes, it means that the last entry in the day is in correlation with the visit
                        # however, the iterator won't skip because of it. This if makes sure, that all the days get linked
                        # properly.
                        if p2+1 == entries_amount:
                            # TODO: Remove duplicate code?
                            # Using else, so it passes even when the for is broken from inside
                            # Calculate single point for visit
                            # avg_location = center_point(visit['list_of_latitudes'], visit['list_of_longitudes'])
                            # visit['latitude'] = avg_location[0]
                            # visit['longitude'] = avg_location[1]
                            # TODO: Do proper averages
                            visit['latitude'] = latitude_1
                            visit['longitude'] = longitude_1
                            visit['starting_time_debug'] = datetime.fromtimestamp(visit['starting_time_timestampS']).strftime('%Y-%m-%d %H:%M:%S')
                            visit['ending_time_debug'] = datetime.fromtimestamp(visit['ending_time_timestampS']).strftime('%Y-%m-%d %H:%M:%S')
                            visit['difference_time_debug'] = visit['ending_time_timestampS'] - visit['starting_time_timestampS']
                            if visit['ending_time_timestampS'] - visit['starting_time_timestampS'] > 0:
                                visit['classification'] = 'visit'
                            else:
                                visit['classification'] = 'travel'

                            visits.append(visit)
                            counter += 1
                            break

                    # Calculate single point for visit
                    # avg_location = center_point(visit['list_of_latitudes'], visit['list_of_longitudes'])
                    # visit['latitude'] = avg_location[0]
                    # visit['longitude'] = avg_location[1]
                    # TODO: Do proper averages
                    visit['latitude'] = latitude_1
                    visit['longitude'] = longitude_1
                    visit['starting_time_debug'] = datetime.fromtimestamp(visit['starting_time_timestampS']).strftime('%Y-%m-%d %H:%M:%S')
                    visit['ending_time_debug'] = datetime.fromtimestamp(visit['ending_time_timestampS']).strftime('%Y-%m-%d %H:%M:%S')
                    visit['difference_time_debug'] = visit['ending_time_timestampS'] - visit['starting_time_timestampS']
                    if visit['ending_time_timestampS'] - visit['starting_time_timestampS'] > 0:
                        visit['classification'] = 'visit'
                    else:
                        visit['classification'] = 'travel'

                    visits.append(visit)
                    counter += 1

                # Write visits of the day at the end of the day processing
                timeline_visits[year][month][day]['visits'] = visits
                print ("[visits]Processed a day.")
                if len(visits) > 10:
                    warn += 1
                    print ("[visits][WARN] Day has more than 10 visits, that is suspicious. The day was: " + str(day) + "." + str(month) + "." + str(year))
                elif len(visits) == 0:
                    warn += 1
                    print ("[visits][WARN] Day has zero visits. The day was: " + str(day) + "." + str(month) + "." + str(year))


            print ("[visits]Processed a month.")

        print ("[visits]Processed a year.")

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    timeline_visits['meta']['count'] = counter
    print ("[visits]Found " + str(counter) + " different visits. Finished in " + str(difference_time_seconds) + ".")
    print ("[visits][WARN-RESULT] There were " + str(warn) + " days with more than 10 visits or with 0 visits. See log.")

    return timeline_visits
